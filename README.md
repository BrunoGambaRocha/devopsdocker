# DevOps Docker

<hr>

- Instrutor: João Vitor Sell
- Plataforma de Ensino: [Cloud Treinamentos](https://academy.cloudtreinamentos.com/?brunogambarocha)
- Curso: Formação DevOps (Turma 3) + Extras



<hr>

##  Objetivo

- Repositório para a prática da formação DevOps da Cloud Treinamentos.
- Entender a cultura DevOps (Pessoas, Processos e Ferramentas).



<hr>

## Autor

- Bruno Gamba Rocha
- [https://www.linkedin.com/in/bruno-gamba-rocha](https://www.linkedin.com/in/bruno-gamba-rocha/)



<hr>

## Tecnologias Utilizadas

- [Serviços AWS](https://aws.amazon.com/pt/)
- [GitLab](https://gitlab.com/BrunoGambaRocha/)
- [Docker](https://www.docker.com/)
- [Docker Toolbox - Windows 8.1](https://docs.docker.com/toolbox/)



<hr>

### Docker Management Commands:
- [ ] builder     Manage builds
- [ ] config      Manage Docker configs
- [ ] container   Manage containers
- [ ] context     Manage contexts
- [ ] image       Manage images
- [x] network     Manage networks

   `docker network ls`

   `docker network create mynetwork`

- [ ] node        Manage Swarm nodes
- [ ] plugin      Manage plugins
- [ ] secret      Manage Docker secrets
- [ ] service     Manage services
- [ ] stack       Manage Docker stacks
- [ ] swarm       Manage Swarm
- [ ] system      Manage Docker
- [ ] trust       Manage trust on Docker images
- [x] volume      Manage volumes

    `docker volume ls`



<hr>

### Docker Commands
 - [ ] attach = Attach local standard input, output, and error streams to a running container
 - [ ] build = Build an image from a Dockerfile
 - [x] commit = Create a new image from a container's changes

    `docker commit webserver mywebserver`

 - [ ] cp = Copy files/folders between a container and the local filesystem
 - [ ] create = Create a new container
 - [ ] diff = Inspect changes to files or directories on a container's filesystem
 - [ ] events = Get real time events from the server
 - [x] exec = Run a command in a running container

    `docker exec CONTAINER_ID COMMAND`

    `docker exec webserver ls`

    `docker exec -it webserver bash`

 - [ ] export = Export a container's filesystem as a tar archive
 - [ ] history = Show the history of an image
 - [x] images = List images

    `docker images`
 
 - [ ] import = Import the contents from a tarball to create a filesystem image
 - [ ] info = Display system-wide information
 - [x] inspect = Return low-level information on Docker objects

    `docker inspect webserver`

 - [ ] kill = Kill one or more running containers
 - [ ] load = Load an image from a tar archive or STDIN
 - [ ] login = Log in to a Docker registry
 - [ ] logout = Log out from a Docker registry
 - [ ] logs = Fetch the logs of a container
 - [ ] pause = Pause all processes within one or more containers
 - [ ] port = List port mappings or a specific mapping for the container
 - [x] ps = List containers

    `docker ps -a`

 - [ ] pull = Pull an image or a repository from a registry
 - [ ] push = Push an image or a repository to a registry
 - [ ] rename = Rename a container
 - [x] restart = Restart one or more containers

    `docker restart webserver`

 - [x] rm = Remove one or more containers
    
    `docker rm -f CONTAINER_ID`
 
 - [x] rmi = Remove one or more images
    
    `docker rmi -f IMAGE_ID`
 
 - [x] run = Run a command in a new container

    `docker run hello-world`
 
    `docker run -it ubuntu bash`

    `docker run -d -p 8080:80 --name webserver nginx`

    `docker run -d -p 8080:80 -v /usr/share/nginx/html/ --name webserver nginx`

    `docker run -itd -p 8080:80 -v /html/:/usr/share/nginx/html/ --name webserver nginx`
    
    `docker run -itd -p 8081:80 -v /html/:/usr/share/nginx/html/ --name mywebserver nginx`

    `docker run --network mynetwork --name mysqldb -e MYSQL_ROOT_PASSWORD=pwd12345 -e MYSQL_DATABASE=wordpress -d mysql`

    `docker run -itd --name wordpress --network mynetwork \
-e WORDPRESS_DB_HOST=mysqldb:3306 \
-e WORDPRESS_DB_USER=root \
-e WORDPRESS_DB_PASSWORD=pwd12345 \
-e WORDPRESS_DB_NAME=wordpress \
-p 8080:80 \
wordpress`
 
 - [ ] save = Save one or more images to a tar archive (streamed to STDOUT by default)
 - [ ] search = Search the Docker Hub for images
 - [ ] start = Start one or more stopped containers
 - [ ] stats = Display a live stream of container(s) resource usage statistics
 - [x] stop = Stop one or more running containers

    `docker stop webserver`

 - [ ] tag = Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
 - [ ] top = Display the running processes of a container
 - [ ] unpause = Unpause all processes within one or more containers
 - [ ] update = Update configuration of one or more containers
 - [x] version = Show the Docker version information

    `docker --version`

 - [ ] wait = Block until one or more containers stop, then print their exit codes